﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuiCheunOBehaviour : MonoBehaviour
{
    #region Enums and Constants
    private const float ROTATE_SPEED = 1f;
    private const string RESERVATION = "https://www.tmf.hk/%E4%B8%8A%E9%96%80%E5%BA%A6%E5%B0%BA/";
    private const string INTERVIEW = "https://www.tmf.hk/%E9%9D%A2%E8%AB%87%E6%9F%A5%E8%A9%A2/";
	private const int STEP_BUTTON_GAP = 355;
    public enum RotateDirection
    {
        None, Clockwise, Anticlockwise
    }
    public enum ScoARMode
    {
        Normal, Sales
    }
    #endregion

    [Header("Object Settings")]
    public float rotateSpeed;
    public float pinchSpeed;

    private Vector3 buildingObjectCurrentScale;
    private float buildingObjectOriginalScale;
    private float buildingObjectMaxScale;
    private float buildingObjectMinScale;

    private RotateDirection rotateDirection = RotateDirection.None;
    private Vector3 buildingObjectCurrentRotation;
    private Vector3 buildingObjectOriginalRotation;
	public bool isReady = false;
	private GameObject currentHitObject;
	public PlayAnimationController animationController ;

    



	public void CallForSetUp ()
	{
		buildingObjectCurrentScale = ARGUIController1.thisObject.transform.localScale;
		buildingObjectOriginalScale = ARGUIController1.thisObject.transform.localScale.x;
		buildingObjectMaxScale = buildingObjectCurrentScale.x * 2;
		buildingObjectMinScale = buildingObjectCurrentScale.x / 2;

		buildingObjectOriginalRotation = buildingObjectCurrentRotation = ARGUIController1.thisObject.transform.localEulerAngles;
	}
 
   
	// Update is called once per frame
    void Update()
	{  
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit hit;
			if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit)) 
			{
				if (hit.collider.gameObject != null) 
				{
				//	CCScreenLogger.LogStatic ("mouseDownhit : " + hit.collider.gameObject.transform.parent.gameObject.name, 3);
					animationController.PlayAnimation (hit.collider.gameObject.transform.parent.gameObject);
				}
			}
		}

       
		//if (Input.GetMouseButtonDown (0) /*&& !UICamera.isOverUI*/) {
		//	RaycastHit hit;
			//if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit)) 
			//{
				//currentHitObject = hit.collider.gameObject;
				//Debug.Log ("currentHitObject " + currentHitObject.name);
			//}
		//}
		//if (Input.GetMouseButtonUp(0) /*&& !UICamera.isOverUI*/)
       // {
         //   RaycastHit hit;
          //  if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
           // {

			/*	if (hit.collider.gameObject.Equals (currentHitObject)) 
				{
					selectVRPin (hit);
					GameObject selectedObject = hit.collider.gameObject;
					/*ThreeDObjectButton threeDBtnt = selectedObject.GetComponentInParent<ThreeDObjectButton> ();
					if (threeDBtnt != null) 
					{
                        if (threeDBtnt.action == ThreeDObjectButton.Action.Play_Animation)
                        {
                            animationController.PlayParentAnimation(selectedObject);
                        }
						//threeDBtnt.OnButtonClicked ();
						//arBtnCallAnimList.Add (threeDBtnt);
					}
				}*/
           // }
			//currentHitObject = null;
       // }
		//if (Input.touchCount > 1) 	
		//{
		//	currentHitObject = null;
		//}
        
		//animationController.UpdateAnimationList ();
//		// if animation is playing, if this obj should not here, disable it
//		for(int i = 0; i < playingAnimationButtonList.Count; i++)
//		{
//			if(!playingAnimationButtonList[i].GetComponentInParent<AnimationProperties>().isAnimating)
//			{
//				playingAnimationButtonList[i].gameObject.SetActive(true);
//
//				playingAnimationButtonList.RemoveAt(i);
//			}
//		}
    }



    /// <summary>
    /// select furniture
    /// </summary>
    /// <param name="hit"></param>
   /* private void selectFurniture(RaycastHit hit)
    {
        deselectFurniture(false);
    

        if (currentFurniture == null/)
        {
            currentFurniture = hit.collider.gameObject;
            currentFurnitureName = currentFurniture.name.Substring(0, currentFurniture.name.IndexOf("_"));
            currentFurniture.AddComponent<SelectedFurnitureEmissionBlink>();

            if (furnitureVariationIndexes.TryGetValue(currentFurnitureName, out currentVariationIndex))
            {
                currentVariationList = furnitureVariations[currentFurnitureName];
            }
            else
            {
                currentVariationIndex = 0;
                furnitureVariationIndexes.Add(currentFurnitureName, currentVariationIndex);

                currentVariationList = new List<GameObject>();
                furnitureVariations.Add(currentFurnitureName, currentVariationList);
            }
        }
    }

    private void deselectFurniture(bool isMoveFurnitureSlidebarIn = true)
    {
        if (currentFurniture != null)
        {
            currentFurniture.GetComponent<SelectedFurnitureEmissionBlink>().resetAndRemove();
            currentFurnitureName = "";
            currentFurniture = null;

            englishLabel.text = "";
            chineseLabel.text = "";
        }
    }*/



    public void scaleObject(float scaleSize)
	{
		if (ARGUIController1.thisObject != null)
		{
			float adjustedScaleSize = scaleSize * Time.deltaTime * pinchSpeed;
			buildingObjectCurrentScale.x += adjustedScaleSize;

			if (buildingObjectCurrentScale.x >= buildingObjectMaxScale) {
				buildingObjectCurrentScale.x = buildingObjectMaxScale;
			} else if (buildingObjectCurrentScale.x <= buildingObjectMinScale) {
				buildingObjectCurrentScale.x = buildingObjectMinScale;
			}

			buildingObjectCurrentScale.z = buildingObjectCurrentScale.y = buildingObjectCurrentScale.x;
			ARGUIController1.thisObject.transform.localScale = buildingObjectCurrentScale;
		
		} 
    }

    public void rotateObject(float rotatedAngle)
	{
		if (ARGUIController1.thisObject != null) 
		{
			ARGUIController1.thisObject.transform.Rotate (new Vector3 (0, rotatedAngle * ROTATE_SPEED, 0), Space.World);
		}  

    }

    public void setRotateClockwise()
    {
        if (rotateDirection == RotateDirection.None)
        {
            rotateDirection = RotateDirection.Clockwise;
        }
    }

    public void setRotateAntiClockwise()
    {
        if (rotateDirection == RotateDirection.None)
        {
            rotateDirection = RotateDirection.Anticlockwise;
        }
    }

    public void unsetRotate()
    {
        if (rotateDirection != RotateDirection.None)
        {
            rotateDirection = RotateDirection.None;
        }
    }

    public void resetScale()
    {
        buildingObjectCurrentScale.x = buildingObjectCurrentScale.y = buildingObjectCurrentScale.z = buildingObjectOriginalScale;
		ARGUIController1.thisObject.transform.localScale = buildingObjectCurrentScale;
    }

    public void resetRotation()
    {
		ARGUIController1.thisObject.transform.localEulerAngles = buildingObjectOriginalRotation;
    }

    private void rotateModel(int rotateDirection)
    {
        buildingObjectCurrentRotation.y += (rotateSpeed * rotateDirection * Time.deltaTime);
		ARGUIController1.thisObject.transform.localEulerAngles = buildingObjectCurrentRotation;
    }



   
}




