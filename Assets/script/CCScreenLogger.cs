﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using UnityEngine;

/// <summary>
/// Custom logger to log output to screen for quicker debugging
/// <para>
/// Extend this class to implement its core behaviour. Override functions to customize functionality.
/// </para>
/// </summary>
public class CCScreenLogger : MonoBehaviour
{
    private class CCScreenLoggerProperties
    {
        public string[] rollingDebugLogLines;
        public string[] staticDebugLogLines;

        private int _maxRollingLogSize = 0;
        public int maxRollingLogSize
        {
            get
            {
                return _maxRollingLogSize;
            }
            private set
            {
                _maxRollingLogSize = value;
            }
        }

        private int _rollingLogLineIndex = -1;
        public int rollingLogLineIndex
        {
            get
            {
                return _rollingLogLineIndex;
            }
            private set
            {
                _rollingLogLineIndex = value;
            }
        }

        /// <summary>
        /// Increment rolling log counter
        /// </summary>
        public void incrementRollingIndexCounter()
        {
            rollingLogLineIndex++;
            if (rollingLogLineIndex >= maxRollingLogSize)
            {
                rollingLogLineIndex = 0;
            }
        }

        /// <param name="numberOfRollingLines">
        /// The number of rolling debug lines
        /// </param>
        /// <param name="numberOfStaticLines">
        /// The number of static debug lines
        /// </param>
        public CCScreenLoggerProperties(int numberOfRollingLines, int numberOfStaticLines)
        {
            _maxRollingLogSize = numberOfRollingLines;
            rollingDebugLogLines = new string[numberOfRollingLines];
            staticDebugLogLines = new string[numberOfStaticLines];
        }
    }

    #region Enums and Constants
    private static string VARIABLE_PLACEHOLDER = "%VAR";
    #endregion

    #region Public Variables
    [Header("Log Settings")]
    [Tooltip("Number of rolling debug lines to show on screen")]
    [Range(1, 10)]
    public int numberOfRollingLines = 5;
    [Tooltip("Number of static debug lines to show on screen")]
    [Range(1, 10)]
    public int numberOfStaticLines = 5;
    public bool isLogToDebugLog = false;

    [Header("Log Layout Settings")]
    [Tooltip("In pixels")]
    [Range(0, 100)]
    public int spacingBetweenLines = 40;
    [Tooltip("In pixels")]
    [Range(0, 500)]
    public int offsetFromEdge = 0;

    [Header("Log Style Settings")]
    [Range(10, 50)]
    public int fontSize = 20;
    public FontStyle fontStyle = FontStyle.Normal;
    public Color textColor = new Color(1, 1, 1);

    [Header("Rolling Log Current Line Style Settings")]
    [Range(10, 50)]
    public int currentLineFontSize = 22;
    public FontStyle currentLineFontStyle = FontStyle.Bold;
    public Color currentLineTextColor = new Color(0.95703125f, 0.21875f, 0.21875f);
    #endregion

    #region Protected Variables
    private CCScreenLoggerProperties logProperties;
    #endregion

    #region Private Variables
    private static CCScreenLogger Instance;

    private static int NumberOfRollingLines;
    private static int NumberOfStaticLines;
    private static bool IsLogToDebugLog;

    private static int SpacingBetweenLines;
    private static int OffsetFromEdge;

    private static int FontSize;
    private static FontStyle FontStyle;
    private static Color TextColor;

    private static int CurrentLineFontSize;
    private static FontStyle CurrentLineFontStyle;
    private static Color CurrentLineTextColor;

    private GUIStyle _style;
    public GUIStyle style
    {
        get
        {
            return _style;
        }
        private set
        {
            _style = value;
        }
    }

    private GUIStyle _currentLineStyle;
    public GUIStyle currentLineStyle
    {
        get
        {
            return _currentLineStyle;
        }
        private set
        {
            _currentLineStyle = value;
        }
    }

    private bool isInitialized = false;
    #endregion

    /// <summary>
    /// Core scene controller must call this to initialize the logger
    /// </summary>
    //private static void Initialize (int numberOfRollingLines, int numberOfStaticLines,
    //    int fontSize, FontStyle fontStyle, Color textColor,
    //    int currentLineFontSize, FontStyle currentLineFontStyle, Color currentLineTextColor)
    private void initialize()
    {
        logProperties = new CCScreenLoggerProperties(numberOfRollingLines, numberOfStaticLines);

        style = new GUIStyle();
        style.fontSize = fontSize;
        style.fontStyle = fontStyle;
        style.normal.textColor = textColor;

        currentLineStyle = new GUIStyle();
        currentLineStyle.fontSize = currentLineFontSize;
        currentLineStyle.fontStyle = currentLineFontStyle;
        currentLineStyle.normal.textColor = currentLineTextColor;

        isInitialized = true;
    }

    /// <summary>
    /// get instance
    /// </summary>
    /// <returns></returns>
    private static CCScreenLogger GetInstance()
    {
        if (Instance == null)
        {
            Instance = FindObjectOfType<CCScreenLogger>();
        }

        if (!Instance.isInitialized)
        {
            Instance.initialize();
        }

        return Instance;
    }

    /// <summary>
    /// MonoBehaviour OnGUI() function.
    /// </summary>
    void OnGUI ()
    {
        CCScreenLogger instance = GetInstance();
        int y = -spacingBetweenLines;

        GUI.Label(new Rect(offsetFromEdge, y += spacingBetweenLines, 100, 20), "Rolling Logs:", style);

        for (int i = 0; i < instance.logProperties.rollingDebugLogLines.Length; i++)
        {
            string debugLine = instance.logProperties.rollingDebugLogLines[i];
            GUI.Label(new Rect(offsetFromEdge, y += spacingBetweenLines, 100, 20), (string.IsNullOrEmpty(debugLine) ? "" : debugLine), (instance.logProperties.rollingLogLineIndex == i ? currentLineStyle : style));
        }

        GUI.Label(new Rect(offsetFromEdge, y += spacingBetweenLines, 100, 20), "Static Logs:", style);
        
        for (int i = 0; i < instance.logProperties.staticDebugLogLines.Length; i++)
        {
            string debugLine = instance.logProperties.staticDebugLogLines[i];
            GUI.Label(new Rect(offsetFromEdge, y += spacingBetweenLines, 100, 20), "[" + i + "] = " + (string.IsNullOrEmpty(debugLine) ? "" : debugLine), style);
        }
    }

    #region Rolling Log Functions
    /// <summary>
    /// Logs the message onto the rolling debug log lines on screen.
    /// </summary>
    /// <param name="message">
    /// The log message to display
    /// </param>
    /// <param name="isLogToDebugLogOverride">
    /// Override logger global setting to output to standard Debug log as well
    /// </param>
    private static void _LogRolling (string message, bool isLogToDebugLogOverride)
    {
        CCScreenLogger instance = GetInstance();
        instance.logProperties.incrementRollingIndexCounter();
        Log(message, instance.logProperties.rollingDebugLogLines, instance.logProperties.rollingLogLineIndex, isLogToDebugLogOverride);
    }

    /// <summary>
    /// Logs the specified variable and its value onto the screen. Format logged like ([%VAR]:%VALUE)
    /// </summary>
    /// <param name="variable">
    /// The name of the variable to display
    /// </param>
    /// <param name="value">
    /// The value of the variable to display
    /// </param>
    /// <param name="isLogToDebugLogOverride">
    /// Override logger global setting to output to standard Debug log as well
    /// </param>
    public static void LogRolling (string variable, string value, bool isLogToDebugLogOverride = false)
    {
        string message = "([" + variable + "]:" + value + ")";
        _LogRolling(message, isLogToDebugLogOverride);
    }

    /// <summary>
    /// Logs the message onto the rolling debug log lines on screen.
    /// <para>
    /// Place one or more %VAR in the string to replace them with the variable value from the list (in order of the array) with format: %VAR -> [%VALUE]
    /// </para>
    /// <para>
    /// If there are more %VAR tags than variables, the remainder will be unused. If there are less, then the remaining variables will not be parsed
    /// </para>
    /// </summary>
    /// <param name="message">
    /// The log message to display
    /// </param>
    /// <param name="listOfVariables">
    /// Array of variables to parse
    /// </param>
    /// <param name="isLogToDebugLogOverride">
    /// Override logger global setting to output to standard Debug log as well
    /// </param>
    public static void LogRolling (string message, bool isLogToDebugLogOverride = false, params object[] listOfVariables)
    {
        StringBuilder sb = new StringBuilder();
        int listIndex = 0;
        int startIndex = 0;

        while (true)
        {
            int nextIndex = message.IndexOf(VARIABLE_PLACEHOLDER, startIndex);
            if (nextIndex != -1 && listOfVariables.Length > 0 && listIndex < listOfVariables.Length)
            {
                object variable = listOfVariables[listIndex];
                sb.Append(message.Substring(startIndex, nextIndex));
                sb.Append("[" + variable + "]");

                listIndex++;
                startIndex = nextIndex + VARIABLE_PLACEHOLDER.Length;
            }
            else
            {
                sb.Append(message.Substring(startIndex));
                break;
            }
        }

        _LogRolling(sb.ToString(), isLogToDebugLogOverride);
    }
    #endregion

    #region Static Log Functions
    /// <summary>
    /// Logs the message onto the static debug log lines on screen.
    /// </summary>
    /// <param name="message">
    /// The log message to display
    /// </param>
    /// <param name="staticLineIndex">
    /// The index of the static lines list to log to
    /// </param>
    /// <param name="isLogToDebugLogOverride">
    /// Override logger global setting to output to standard Debug log as well
    /// </param>
    private static void _LogStatic (string message, int staticLineIndex, bool isLogToDebugLogOverride)
    {
        CCScreenLogger instance = GetInstance();

        Log(message, instance.logProperties.staticDebugLogLines, staticLineIndex, isLogToDebugLogOverride);
    }

    /// <summary>
    /// Logs the specified variable and its value onto the screen. Format logged like ([%VAR]:%VALUE)
    /// </summary>
    /// <param name="variable">
    /// The name of the variable to display
    /// </param>
    /// <param name="value">
    /// The value of the variable to display
    /// </param>
    /// <param name="staticLineIndex">
    /// The index of the static lines list to log to
    /// </param>
    /// <param name="isLogToDebugLogOverride">
    /// Override logger global setting to output to standard Debug log as well
    /// </param>
    public static void LogStatic (string variable, string value, int staticLineIndex, bool isLogToDebugLogOverride = false)
    {
        string message = "([" + variable + "]:" + value + ")";
        _LogStatic(message, staticLineIndex, isLogToDebugLogOverride);
    }

    /// <summary>
    /// Logs the message onto the static debug log lines on screen.
    /// <para>
    /// Place one or more %VAR in the string to replace them with the variable value from the list (in order of the array) with format: %VAR -> [%VALUE]
    /// </para>
    /// <para>
    /// If there are more %VAR tags than variables, the remainder will be unused. If there are less, then the remaining variables will not be parsed
    /// </para>
    /// </summary>
    /// <param name="message">
    /// The log message to display
    /// </param>
    /// <param name="staticLineIndex">
    /// The index of the static lines list to log to
    /// </param>
    /// <param name="isLogToDebugLogOverride">
    /// Override logger global setting to output to standard Debug log as well
    /// </param>
    /// <param name="listOfVariables">
    /// Array of variables to parse
    /// </param>
    public static void LogStatic (string message, int staticLineIndex, bool isLogToDebugLogOverride = false, params object[] listOfVariables)
    {
        StringBuilder sb = new StringBuilder();
        int listIndex = 0;
        int startIndex = 0;

        while (true)
        {
            int nextIndex = message.IndexOf(VARIABLE_PLACEHOLDER, startIndex);
            if (nextIndex != -1 && listOfVariables.Length > 0 && listIndex < listOfVariables.Length)
            {
                object variable = listOfVariables[listIndex];
                sb.Append(message.Substring(startIndex, nextIndex - startIndex));
                sb.Append("[" + variable + "]");

                listIndex++;
                startIndex = nextIndex + VARIABLE_PLACEHOLDER.Length;
            }
            else
            {
                sb.Append(message.Substring(startIndex));
                break;
            }
        }

        _LogStatic(sb.ToString(), staticLineIndex, isLogToDebugLogOverride);
    }
    #endregion

    #region Common Log Functions
    /// <summary>
    /// Logs the message into the given log list at the provided index
    /// </summary>
    /// <param name="message">
    /// The log message to display
    /// </param>
    /// <param name="logList">
    /// The list to log into
    /// </param>
    /// <param name="indexToLog">
    /// The index in the list to log
    /// </param>
    /// <param name="isLogToDebugLogOverride">
    /// Override logger global setting to output to standard Debug log as well
    /// </param>
    private static void Log (string message, string[] logArray, int indexToLog, bool isLogToDebugLogOverride)
    {
        CCScreenLogger instance = GetInstance();

        logArray[indexToLog] = message;

        // If override not specified, use global setting
        if (instance.isLogToDebugLog || isLogToDebugLogOverride)
        {
            Debug.Log(message);
        }
    }
    #endregion

    #region Private Functions
    /// <summary>
    /// Returns the name of the passed in class member
    /// </summary>
    /// <returns>
    /// A string containing the member name
    /// </returns>
    /// <param name="memberExpression">
    /// The member to parse
    /// </param>
    public static string GetMemberName<T> (Expression<Func<T>> memberExpression)
    {
        MemberExpression expressionBody = (MemberExpression)memberExpression.Body;
        return expressionBody.Member.Name;
    }
    #endregion
}
